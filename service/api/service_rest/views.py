# from django.shortcuts import render
from .models import Technician, Appointment, AutomobileVO
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import (
    AutomobileVOEncoder,
    TechnicianEncoder,
    AppointmentEncoder,
    )


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technician_list = Technician.objects.all()
        return JsonResponse(
            {"technician_list": technician_list},
            encoder=TechnicianEncoder,
        )
    else:
        # handles a POST request
        content = json.loads(request.body)
        try:
            technicians = Technician.objects.create(**content)
            return JsonResponse(
                technicians,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"error": "Technician does not exist."},
                status=404
            )


@require_http_methods(["DELETE"])
def api_technician_delete(request, employee_id):
    if request.method == "DELETE":

        try:
            technician = Technician.objects.get(employee_id=employee_id)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician Does not exist"},
                status=404
            )


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder
        )
    else:
        # handle a POST request
        content = json.loads(request.body)

        try:
            technician_id = content["technician"]
            technician = Technician.objects.get(employee_id=technician_id)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"error": "Technician not found"},
                status=400
            )

        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_appointment_delete(request, vin):
    if request.method == "DELETE":

        try:
            appointment = Appointment.objects.get(vin=vin)
            appointment.delete()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status=404
            )


@require_http_methods(["PUT"])
def api_cancel_appointment(request, vin):
    if request.method == "PUT":
        appointment = Appointment.objects.get(vin=vin)
        appointment.cancel()
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )


@require_http_methods(["PUT"])
def api_finish_appointment(request, vin):
    if request.method == "PUT":
        appointment = Appointment.objects.get(vin=vin)
        appointment.finish()
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )


@require_http_methods(["GET"])
def api_get_automobileVO(request):
    if request.method == "GET":
        automobileVO = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobileVO": automobileVO},
            encoder=AutomobileVOEncoder
        )
