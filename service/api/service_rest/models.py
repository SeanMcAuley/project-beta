from django.db import models
from django.urls import reverse


class Technician(models.Model):
    first_name = models.CharField(max_length=100, unique=True)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=10, unique=True)


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def get_api_url(self):
        return reverse("api_automobileVO", kwargs={"vin": self.vin})


class Appointment(models.Model):

    status_choices = [
        ("created", "created"),
        ("finished", "finished"),
        ("canceled", "canceled"),
    ]

    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    status = models.CharField(
        max_length=50,
        choices=status_choices,
        default="created"
    )
    vin = models.CharField(max_length=17, unique=True)
    customer = models.CharField(max_length=50)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )

    def cancel(self):
        self.status = "canceled"
        self.save()

    def finish(self):
        self.status = "finished"
        self.save()

    def __str__(self):
        return self.customer
