from django.urls import path
from .views import (
    api_list_technicians,
    api_list_appointments,
    api_technician_delete,
    api_appointment_delete,
    api_cancel_appointment,
    api_finish_appointment,
    api_get_automobileVO
)

urlpatterns = [
     path(
        "technicians/",
        api_list_technicians,
        name="api_list_technicians"
          ),
     path(
         "technicians/<str:employee_id>/",
         api_technician_delete,
         name="api_delete_technician"
          ),
     path(
        "appointments/",
        api_list_appointments,
        name="api_list_technicians",
          ),
     path(
         "appointments/<str:vin>/",
         api_appointment_delete,
         name="api_appointment_delete",
          ),
     path(
         "appointments/<str:vin>/cancel/",
         api_cancel_appointment,
         name="api_cancel_appointment",
         ),
     path(
         "appointments/<str:vin>/finish/",
         api_finish_appointment,
         name="api_finish_appointment",
         ),
     path(
          "automobileVO/",
          api_get_automobileVO,
          name="api_get_automobileVO"
          ),
]
