# Generated by Django 4.0.3 on 2023-06-08 14:38

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0002_rename_vin_automobilevo_import_vin'),
    ]

    operations = [
        migrations.RenameField(
            model_name='salesperson',
            old_name='employeee_id',
            new_name='employee_id',
        ),
    ]
