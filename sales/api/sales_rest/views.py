from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .models import (
    AutomobileVO,
    Salesperson,
    Customer,
    Sale,
)
from .encoders import (
    SalespersonEncoder,
    CustomerEncoder,
    SaleEncoder,
)


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople_list = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople_list": salespeople_list},
            encoder=SalespersonEncoder
        )
    else:
        content = json.loads(request.body)

        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_delete_salesperson(request, employee_id):
    if request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(employee_id=employee_id)
            salesperson.delete()
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )

        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=404
            )


@require_http_methods(["GET", "POST"])
def api_list_customer(request):
    if request.method == "GET":
        customers_list = Customer.objects.all()
        return JsonResponse(
            {"customers_list": customers_list},
            encoder=CustomerEncoder
        )
    else:
        content = json.loads(request.body)

        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_delete_customer(request, id):
    if request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=id)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )

        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=404
            )


@require_http_methods(["GET", "POST"])
def api_list_sale(request):
    if request.method == "GET":
        sale_list = Sale.objects.all()
        return JsonResponse(
            {"sale_list": sale_list},
            encoder=SaleEncoder
        )
    else:
        new_content = {}
        content = json.loads(request.body)

# ===================== Automobile input ===========================
        try:
            import_vin = content["import_vin"]
            automobile = AutomobileVO.objects.get(import_vin=import_vin)
            if automobile.sold is False:
                AutomobileVO.objects.filter(import_vin=import_vin).update(sold=True)

                new_content["automobile"] = automobile
            else:
                return JsonResponse(
                    {"message": "Automobile has already been sold"},
                    status=400
                )
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Vin Number"},
                status=404,
            )
# ===================== Customer input ===========================
        try:
            address_input = content["address_input"]
            customer = Customer.objects.get(address=address_input)
            new_content["customer"] = customer
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Customer Address"},
                status=404,
            )
# ===================== Salesperson input ===========================
        try:
            employee_id_input = content["employee_id_input"]
            salesperson = Salesperson.objects.get(employee_id=employee_id_input)
            new_content["salesperson"] = salesperson
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Employee ID"},
                status=404,
            )

        price = content["price"]
        new_content["price"] = price

        sale = Sale.objects.create(**new_content)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_delete_sale(request, id):
    if request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=id)
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )

        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Sale does not exist"},
                status=404
            )
