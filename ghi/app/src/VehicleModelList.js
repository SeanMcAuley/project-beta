import React, { useEffect, useState } from 'react';

const VehicleModelList = () => {
    const [models, setVehicleModels] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setVehicleModels(data.models);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Picture</th>
                    <th>Manufacturer</th>
                </tr>
            </thead>
            <tbody>
                {models.map(model => {
                    return (
                        <tr key={ model.href }>

                            <td>{ model.name }</td>
                            <td><img src={ model.picture_url } alt="Vehicle Picture" /></td>
                            <td>{ model.manufacturer.name }</td>
                        </tr>
                );
            })}
            </tbody>
        </table>
    );
};


export default VehicleModelList;
