import React, { useEffect, useState } from 'react';

const ServiceAppointmentList = () => {
    const [appointments, setAppointments] = useState([]);
    const [automobiles, setAutomobiles] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/appointments/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
        }
    }

    const fetchAutomobiles = async () => {
        const url = 'http://localhost:8080/api/automobileVO/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.automobileVO);
        } else {
            console.log("Response failed:", response.status, response.statusText);
        }
    };


    const handleCancelAppointment=(appointment) => {
        const url = `http://localhost:8080/api/appointments/${appointment.vin}/cancel/`;
        handlePutRequest(url);
    };

    const handleFinishAppointment=(appointment) => {
        const url = `http://localhost:8080/api/appointments/${appointment.vin}/finish/`;
        handlePutRequest(url);
    };


    const handlePutRequest = (url) => {
        fetch(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({}),
        }).then(response => {
            if (response.ok) {
        } else {
            throw new Error("Request Failed")
            }
        }) .catch(
            error => {
            console.error(error);
        });
    };


    useEffect(() => {
        fetchData();
        fetchAutomobiles();
    }, []);

    const filteredAppointments = appointments.filter(appointment =>
        appointment.status === 'created'
        );

    // check if vin matches automobile VO's vin
    const checkVin = (vin1, vin2) => {
        return vin1 === vin2;
    };

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Is VIP?</th>
                    <th>Customer</th>
                    <th>Date and Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                {filteredAppointments.map((appointment) => {
                    const automobile = automobiles.find((auto) =>
                    checkVin(appointment.vin, auto.vin)
                    );

                    const isVip = automobile && automobile.sold ? 'Yes' : 'No';

                    return (
                        <tr key={ appointment.vin }>

                            <td>{ appointment.vin }</td>
                            <td>{ isVip }</td>
                            <td>{ appointment.customer }</td>
                            <td>{ appointment.date_time}</td>
                            <td>{ appointment.technician.employee_id }</td>
                            <td>{ appointment.reason }</td>
                            <td>
                                <button onClick={()=>
                                    handleCancelAppointment(appointment)}>Cancel</button>
                                <button onClick={()=>
                                    handleFinishAppointment(appointment)}>Finish</button>
                            </td>
                        </tr>
                );
            })}
            </tbody>
        </table>
    );
};


export default ServiceAppointmentList;
