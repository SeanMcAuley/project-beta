import React, { useEffect, useState } from 'react';

const AutomobileList = () => {
    const [automobiles, setAutomobiles] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const renderSoldStatus = (sold) => {
        return sold ? 'Yes' : 'No';
    };

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Color</th>
                    <th>Year</th>
                    <th>VIN</th>
                    <th>Model</th>
                    <th>Sold</th>
                </tr>
            </thead>
            <tbody>
                {automobiles.map(automobile => {
                    return (
                        <tr key={ automobile.href }>

                            <td>{ automobile.color }</td>
                            <td>{ automobile.year }</td>
                            <td>{ automobile.vin }</td>
                            <td>{ automobile.model.name }</td>
                            <td>{renderSoldStatus(automobile.sold) }</td>
                        </tr>
                );
            })}
            </tbody>
        </table>
    );
};


export default AutomobileList;
