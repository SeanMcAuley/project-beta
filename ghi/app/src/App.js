import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerList from './ListManufacturer';
import ManufacturerForm from './ManufacturerForm';
import AutomobileForm from './AutomobileForm';
import VehicleModelList from './VehicleModelList';
import VehicleModelForm from './VehicleModelForm';
import AutomobileList from './AutomobileList';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import ServiceAppointmentForm from './ServiceAppointmentForm';
import CreateCustomer from './CustomerForm';
import CustomerList from './CustomerList';
import CreateSalesperson from './SalespersonForm';
import SalespersonList from './SalespersonList';
import SaleList from './SaleList';
import SaleForm from './SaleForm';
import SalespersonHistory from './SalespersonHistory';
import ServiceAppointmentList from './ServiceAppointmentList';
import ServiceHistoryList from './ServiceHistoryList';



function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/ManufacturerList" element={<ManufacturerList />} />
          <Route path="/CreateManufacturer" element={<ManufacturerForm />} />
          <Route path="/CreateAutomobile" element={<AutomobileForm />} />
          <Route path="/VehicleModelList" element={<VehicleModelList />} />
          <Route path="/VehicleModelForm" element={<VehicleModelForm />} />
          <Route path="/AutomobileList" element={<AutomobileList />} />
          <Route path="/TechnicianList" element={<TechnicianList />} />
          <Route path="/TechnicianForm" element={<TechnicianForm />} />
          <Route path="/ServiceAppointmentForm" element={<ServiceAppointmentForm />} />
          <Route path="/CreateCustomer" element={<CreateCustomer />} />
          <Route path="/CustomerList" element={<CustomerList />} />
          <Route path="/CreateSalesperson" element={<CreateSalesperson />} />
          <Route path="/SalespersonList" element={<SalespersonList />} />
          <Route path="/SaleList" element={<SaleList />} />
          <Route path="/SaleForm" element={<SaleForm />} />
          <Route path="/SalespersonHistory" element={<SalespersonHistory />} />
          <Route path="/ServiceAppointmentList" element={<ServiceAppointmentList />} />
          <Route path="/ServiceHistoryList" element={<ServiceHistoryList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
