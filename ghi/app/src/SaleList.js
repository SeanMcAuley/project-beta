import React, { useEffect, useState } from 'react';

const SaleList = () => {
    const [sales, setSales] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/sales/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSales(data.sale_list);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <table className="table table-striped">
        <thead>
          <tr>
            <th>Automobile</th>
            <th>Price</th>
            <th>Customer</th>
            <th>Salesperson</th>
          </tr>
        </thead>
        <tbody>
          {sales.map(sale => {
            return (
              <tr key={sale.automobile.import_vin}>
                <td> { sale.automobile.import_vin }</td>
                <td> { sale.price }</td>
                <td> { sale.customer.first_name } { sale.customer.last_name }</td>
                <td> { sale.salesperson.first_name } { sale.salesperson.last_name }</td>
              </tr>
            );
          })}
        </tbody>
      </table>

    )
}

export default SaleList;
