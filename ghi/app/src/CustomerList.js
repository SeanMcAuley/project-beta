import React, { useEffect, useState } from 'react';

const CustomerList = () => {
    const [customer, setCustomers] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/customers/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers_list);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <table className="table table-striped">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Address</th>
            <th>Phone Number</th>
          </tr>
        </thead>
        <tbody>
          {customer.map(person => {
            return (
              <tr key={person.phone_number}>
                <td> { person.first_name }</td>
                <td> { person.last_name }</td>
                <td> { person.address }</td>
                <td> { person.phone_number }</td>
              </tr>
            );
          })}
        </tbody>
      </table>

    )
}

export default CustomerList;
