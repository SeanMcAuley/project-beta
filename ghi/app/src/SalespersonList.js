import React, { useEffect, useState } from 'react';

const SalespersonList = () => {
    const [salespeople, setSalespeople] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople_list);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <table className="table table-striped">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Employee ID</th>

          </tr>
        </thead>
        <tbody>
          {salespeople.map(person => {
            return (
              <tr key={person.employee_id}>
                <td> { person.first_name }</td>
                <td> { person.last_name }</td>
                <td> { person.employee_id }</td>
              </tr>
            );
          })}
        </tbody>
      </table>

    )
}

export default SalespersonList;
