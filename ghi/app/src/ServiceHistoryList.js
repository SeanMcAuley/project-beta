import React, { useEffect, useState } from 'react';

const ServiceHistoryList = () => {
    const [appointments, setAppointments] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/appointments/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
        }
    }


    useEffect(() => {
        fetchData();
    }, []);

   
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Is VIP?</th>
                    <th>Customer</th>
                    <th>Date and Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                {appointments.map(appointment => {
                    return (
                        <tr key={ appointment.vin }>

                            <td>{ appointment.vin }</td>
                            <td>{appointment.isVIP? 'Yes' : 'No' }</td>
                            <td>{ appointment.customer }</td>
                            <td>{ appointment.date_time}</td>
                            <td>{ appointment.technician.employee_id }</td>
                            <td>{ appointment.reason }</td>
                            <td>{ appointment.status }</td>
                        </tr>
                );
            })}
            </tbody>
        </table>
    );
};


export default ServiceHistoryList;
