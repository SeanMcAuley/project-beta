import { useEffect, useState } from 'react';

const SalespersonHistory = () => {
    const [sales, setSalespersonhistory] = useState([]);

    const fetchData = async () => {
    const url = 'http://localhost:8090/api/sales/';
    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        setSalespersonhistory(data.sale_list);

    }
    }



    useEffect(()=>{
    fetchData();
    }, [])

    return (
        <table className="table table-striped">
        <thead>
        <tr>
            <th colSpan="2" style={{ fontSize: '35px'}}>Salesperson History</th>
        </tr>
        <tr>
            <th>Sales Person</th>
            <th>Customer</th>
            <th>Vin</th>
            <th>Price</th>
        </tr>
        </thead>
        <tbody>
        {sales.map(sale => {
            return (
            <tr key={sale.automobile.import_vin}>
                <td>{sale.salesperson.first_name}{sale.salesperson.last_name}</td>
                <td>{sale.customer.first_name}{sale.customer.last_name}</td>
                <td>{sale.automobile.import_vin}</td>
                <td>{sale.price}</td>
            </tr>
            );
        })}
        </tbody>
        </table>
    );
}

export default SalespersonHistory;
